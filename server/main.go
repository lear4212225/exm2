package main

import (
	"awesomeProject/rabbit"
	"awesomeProject/serverproto"
	"context"
	"log"
	"time"

	"google.golang.org/protobuf/proto"
)

func main() {
	q := rabbit.NewRabbitMQ()
	server(q)

}

func server(q rabbit.Queuer) {
	del, err := q.GetOrder()
	if err != nil {
		log.Fatal(err)
	}

	order := &serverproto.OrderRequest{}
	go func() {
		for msg := range del {
			var status *serverproto.OrderStatusResponse
			if err := proto.Unmarshal(msg.Body, order); err != nil {
				log.Printf("Failed to unmarshal order: %v", err)
				status = &serverproto.OrderStatusResponse{Success: false}
			} else {
				status = &serverproto.OrderStatusResponse{Success: true}
			}

			err = q.SendStatus(context.Background(), status)
			if err := msg.Ack(false); err != nil {
				log.Printf("Error acknowledging message: %s", err)
			} else {
				log.Printf("Acknowledged message")
			}
			log.Println(order.String())

		}
	}()

	time.Sleep(10 * time.Second)

}
