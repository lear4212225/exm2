package main

import (
	"awesomeProject/rabbit"
	"awesomeProject/serverproto"
	"context"
	"log"
	"time"

	"google.golang.org/protobuf/proto"
)

func main() {
	q := rabbit.NewRabbitMQ()
	client(q)

}

func client(q rabbit.Queuer) {
	order := &serverproto.OrderRequest{
		Id:          55,
		ProductName: "hello",
		Quantity:    10,
		TotalPrice:  5,
		Client:      24,
	}

	go func() {
		err := q.SendOrder(context.Background(), order)
		if err != nil {
			log.Fatal(err)
		}
		time.Sleep(1 * time.Second)

		del, err := q.GetStatus()
		msgString := &serverproto.OrderStatusResponse{}
		for msg := range del {
			proto.Unmarshal(msg.Body, msgString)
			if err := msg.Ack(false); err != nil {
				log.Printf("Error acknowledging message: %s", err)
			} else {
				log.Printf("Acknowledged message")
			}
		}
		log.Println(msgString.String())

	}()

	time.Sleep(10 * time.Second)

}
